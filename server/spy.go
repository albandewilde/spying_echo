package main

import "gitlab.com/albandewilde/spying_echo/grpc/spyingechopb"

// spy represent one of people that listen on the server
type spy struct {
	stream spyingechopb.SpyingEcho_SpyServer
}
